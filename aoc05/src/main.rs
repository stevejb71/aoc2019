use std::fs;

fn main() {
  let data = read_file("./input.txt");
  // run(&data, 1);
  run(&data, 5);
}

fn read_file(filename: &str) -> Vec<i32> {
  fs::read_to_string(filename).unwrap()
      .split(",")
      .map(|x| x.parse().unwrap())
      .collect()
}

#[derive(PartialEq, Debug)]
enum Mode {
  Position,
  Immediate
}

fn run(program: &Vec<i32>, input: i32) -> i32 {
  let mut copy = program.clone();
  let mut pc = 0;
  while copy[pc] != 99 {
    pc = process_op(&mut copy, input, pc);
  }
  copy[0]
}

fn process_op(data: &mut Vec<i32>, input: i32, pc: usize) -> usize {
  let op = opcode(data[pc]);
  println!("Processing pc={}, op={}, data[pc]={}", pc, op, data[pc]);
  let modes = modes(data[pc] as usize);
  let read = {|index| read(data, &modes, pc, index)};
  match op {
    1 => {
      let op_code1 = read(1);
      let op_code2 = read(2);
      let out_index = data[pc+3] as usize;
      println!("Executing data[{}] = {} + {}", out_index, op_code1, op_code2);
      data[out_index] = op_code1 + op_code2;
      pc + 4
    },
    2 => {
      let op_code1 = read(1);
      let op_code2 = read(2);
      let out_index = data[pc+3] as usize;
      println!("Executing data[{}] = {} * {}", out_index, op_code1, op_code2);
      data[out_index] = op_code1 * op_code2;
      pc + 4
    }
    3 => {
      let out_index = data[pc+1] as usize;
      println!("Executing data[{}]={}", out_index, input);
      data[out_index] = input;
      pc + 2
    },
    4 => {
      let op_code1 = read(1);
      println!("Output: {}", op_code1);
      pc + 2
    },
    5 => {
      let op_code1 = read(1);
      if op_code1 != 0 {
        println!("Executing jump-if-true on {}, true so set pc={}", op_code1, read(2));
        read(2) as usize
      } else {
        println!("Executing jump-if-true on {}, false so set pc={}", op_code1, pc + 2);
        pc + 3
      }
    },
    6 => {
      let op_code1 = read(1);
      if op_code1 == 0 {
        println!("Executing jump-if-false on {}, true so set pc={}", op_code1, read(2));
        read(2) as usize
      } else {
        println!("Executing jump-if-false on {}, false so set pc={}", op_code1, pc + 2);
        pc + 3
      }
    },
    7 => {
      let op_code1 = read(1);
      let op_code2 = read(2);
      let out_index = data[pc+3] as usize;
      let value = if op_code1 < op_code2 {
        1
      } else {
        0
      };
      println!("Executing {} < {}, setting data[{}]={}", op_code1, op_code2, out_index, value);
      data[out_index] = value;
      pc + 4
    },
    8 => {
      let op_code1 = read(1);
      let op_code2 = read(2);
      let out_index = data[pc+3] as usize;
      let value = if op_code1 == op_code2 {
        1
      } else {
        0
      };
      println!("Executing {} == {}, setting data[{}]={}", op_code1, op_code2, out_index, value);
      data[out_index] = value;
      pc + 4
    },
    _ => panic!("Unhandled op: {}", op)
  }
}

fn read(data: &Vec<i32>, (a, b, c): &(Mode, Mode, Mode), pc: usize, index: usize) -> i32 {
  let mode = match index {
    1 => c,
    2 => b,
    3 => a,
    _ => panic!("Unhandled index for mode {}", index)
  };
  let param = data[pc+index];
  if *mode == Mode::Immediate {
    println!("At index {}, immediate param {}", index, param);
    param
  } else {
    println!("At index {}, position param {} -> {}", index, param, data[param as usize]);
    data[param as usize]
  }
}

fn opcode(instruction: i32) -> usize {
  (instruction % 10) as usize
}

fn modes(instruction: usize) -> (Mode, Mode, Mode) {
  let c = to_mode((instruction / 100) % 10);
  let b = to_mode((instruction / 1000) % 10);
  let a = to_mode((instruction / 10000) % 10);
  (a, b, c)
}

fn to_mode(x: usize) -> Mode {
  match x {
    0 => Mode::Position,
    1 => Mode::Immediate,
    _ => panic!("Unhandled mode {}", x)
  }
}