use std::fs::File;
use std::io::{BufRead, BufReader, Error, ErrorKind, Read};

fn read<R: Read>(io: R) -> Result<Vec<i64>, Error> {
  let br = BufReader::new(io);
  br.lines()
      .map(|line| line.and_then(|v| v.parse().map_err(|e| Error::new(ErrorKind::InvalidData, e))))
      .collect()
}

fn main() -> Result<(), Error> {
  let module_masses = read(File::open("./src/input.txt")?)?;
  part1(&module_masses);
  part2(&module_masses);
  Ok(())
}

fn part1(module_masses: &Vec<i64>) {
  let fuel: i64 = module_masses.iter().map(|m| m / 3 - 2).sum();
  println!("Total fuel for part 1 {}", fuel);
}

fn total_fuel(m: i64) -> i64 {
  let mut total: i64 = 0;
  let mut fuel = m / 3 - 2;
  while fuel > 0 {
    total += fuel;
    fuel = fuel / 3 - 2;
  }
  total
}

fn part2(module_masses: &Vec<i64>) {
  let fuel: i64 = module_masses.iter().map(|m| total_fuel(*m)).sum();
  println!("Total fuel for part 2 {}", fuel);
}
