use std::fs;

fn main() {
  let text = read("./input.txt");
  let layers = text.as_bytes().chunks(25 * 6).collect::<Vec<_>>();
  part1(&layers);
  part2(&layers, 25*6);
}

fn part1(layers: &Vec<&[u8]>) {
  let index_with_most_zeroes = layers.iter().enumerate()
              .min_by_key(|(_i, l)| count_occurrences(l, b'0'))
              .map(|(i, _l)| i)
              .expect("oops");
  let layer = &layers[index_with_most_zeroes];
  let ones_count = count_occurrences(layer, b'1');
  let twos_count = count_occurrences(layer, b'2');
  println!("Part 1: {}", ones_count * twos_count);
}

fn part2(layers: &Vec<&[u8]>, chunk_size: usize) {
  let slices = layers.len() / chunk_size;
  let mut picture: Vec<char> = Vec::new();
  for i in 0..chunk_size {
    let mut pixels: Vec<u8> = Vec::new();
    for j in 0..slices {
      pixels.push(layers[j][i]);
    }
    picture.push(combine(&pixels));
  }
  println!("Part 2: {:?}", picture);
}

fn read(filename: &str) -> String {
  fs::read_to_string(filename).unwrap()
}

fn count_occurrences(xs: &[u8], letter: u8) -> usize {
  xs.iter().filter(|&&x| x == letter).collect::<Vec<_>>().len()
}

fn combine(pixels: &Vec<u8>) -> char {
  let pixel = *pixels.iter().filter(|&&p| p != b'2').nth(0).unwrap_or(&b'2');
  match pixel {
    48 => 'B',
    49 => 'W',
    50 => ' ',
    _ => panic!(format!("unhandled pixel {}", pixel))
  }
}
