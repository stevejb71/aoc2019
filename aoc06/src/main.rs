use std::{
  fs::File,
  io::{BufRead, BufReader}
};
use std::collections::HashMap;
use petgraph::Graph;
use petgraph::algo::astar;
use petgraph::graph::NodeIndex;

fn main() {
    let lines = read("./input.txt");
    let links = parse(&lines);
    part1(&links);
    part2(&links);
}

fn part1(links: &HashMap<&str, &str>) {
  let mut counts: HashMap<&str, u32> = HashMap::new();
  counts.insert("COM", 0);
  let mut total = 0;
  for(planet, _) in links {
    total += count1(planet, &links, & mut counts);
  }
  println!("Part 1: {}", total);
}

fn part2(links: &HashMap<&str, &str>) {
  let mut graph = Graph::<&str, u32>::new();
  let mut names_2_node_ids: HashMap<&str, NodeIndex> = HashMap::new();
  for (planet, sun) in links {
    let planet_id = graph.add_node(&planet);
    names_2_node_ids.insert(&planet, planet_id);
    let sun_id = graph.add_node(&sun);
    names_2_node_ids.insert(&sun, sun_id);
  }
  for (planet, sun) in links {
    let planet_id = names_2_node_ids[planet];
    let sun_id = names_2_node_ids[sun];
    graph.add_edge(planet_id, sun_id, 1);
    graph.add_edge(sun_id, planet_id, 1);
  }
  let you_id = *names_2_node_ids.get("YOU").expect("no YOU");
  let santa_id = *names_2_node_ids.get("SAN").expect("no SAN");
  let (cost, _) = astar(&graph, you_id, |n| n == santa_id, |_| 1, |_| 0).expect("some result");
  println!("Part 2: {}", cost - 2); // -2 as YOU and SAN have edges.
}

fn count1<'a>(planet: &'a str, orbits: &HashMap<&'a str, &'a str>, counts: &mut HashMap<&'a str, u32>) -> u32 {
  if let Some(count) = counts.get(planet) {
    return *count;
  }
  let sun = orbits[planet];
  let count = 1 + count1(sun, orbits, counts);
  counts.insert(planet, count);
  count
}

fn read(filename: &str) -> Vec<String> {
  let file = File::open(filename).expect("no file");
  BufReader::new(file).lines().map(|l| l.expect("bad line")).collect::<Vec<_>>()
}

fn parse<'a>(links: &'a Vec<String>) -> HashMap<&'a str, &'a str> {
  let mut orbits: HashMap<&str, &str> = HashMap::new();
  for link in links {
    let pair = link.split(')').collect::<Vec<_>>();
    orbits.insert(pair[1], pair[0]);
  }
  orbits
}