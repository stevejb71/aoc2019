use std::fs;

fn read_lines(filename: &str) -> Vec<Vec<String>> {
  fs::read_to_string(filename).unwrap()
      .split("\n")
      .map(|l| l.split(",").map(|s| s.to_string()).collect::<Vec<String>>())
      .collect()
}

fn draw1(board: &mut Vec<u32>, size: usize, instruction: &str, cursor: usize, total_len: u32) -> (usize, u32) {
  let mut c = cursor;
  let direction = &instruction[0..1];
  let length = instruction[1..instruction.len()].parse::<u32>().unwrap();
  let sz = size as i32;
  let delta = match direction {
    "U" => -sz,
    "D" => sz,
    "L" => -1,
    "R" => 1,
    _ => panic!()
  };  
  for i in 0..length {
    board[c] = total_len + i;
    let next = c as i32 + delta;
    c = next as usize;
  }
  (c, length)
}

fn draw(instructions: &Vec<String>, size: usize, start: usize) -> Vec<u32> {
  let mut board = vec![0 as u32; size * size];
  let mut cursor = start;
  let mut total_len = 0;
  for i in instructions {
    let (next_cursor, length) = draw1(&mut board, size, i, cursor, total_len);
    cursor = next_cursor;
    total_len += length;
  }
  board
}

fn manhattan_distance(size: usize, p1: usize, p2: usize) -> usize {
  let x1 = p1 % size;
  let y1 = p1 / size;
  let x2 = p2 % size;
  let y2 = p2 / size;
  (x1 as i32 - x2 as i32).abs() as usize + (y1 as i32 - y2 as i32).abs() as usize
}

fn main() {
  let lines = read_lines("./input1.txt");
  let size = 17000;
  let start = (size / 2) * (size / 2);
  let board1 = draw(&lines[0], size, start);
  let board2 = draw(&lines[1], size, start);
  let mut best1 = std::usize::MAX;
  let mut best2 = std::usize::MAX;
  for i in 0..board1.len() {
    if board1[i] != 0 && board2[i] != 0 {
      let dist1 = manhattan_distance(size, start, i);
      if dist1 != 0 && dist1 < best1  {
        best1 = dist1;
      }
      let dist2 = (board1[i] + board2[i]) as usize;
      if dist2 != 0 && dist2 < best2  {
        best2 = dist2;
      }
    }
  }
  println!("Best distance (part 1): {}", best1);
  println!("Best distance (part 2): {}", best2);
}
