fn main() {
    part1();
    part2();
}

fn part1() {
  let num = (145852..616943).filter(|n| part1_is_valid(*n)).collect::<Vec<_>>().len();
  println!("Part 1: {}", num);
}

fn part2() {
  let num = (145852..616943).filter(|n| part2_is_valid(*n)).collect::<Vec<_>>().len();
  println!("Part 2: {}", num);
}

fn part1_is_valid(n: u32) -> bool {
  let text = n.to_string();
  let bytes = text.as_bytes();
  let mut adjacent_same = false;
  for i in 0..5 {
    if bytes[i] == bytes[i+1] {
      adjacent_same = true;
    }
    if bytes[i] > bytes[i+1] {
      return false;
    }
  }
  return adjacent_same;
}

fn part2_is_valid(n: u32) -> bool {
  let text = n.to_string();
  let bytes = text.as_bytes();
  for i in 0..5 {
    if bytes[i] > bytes[i+1] {
      return false;
    }
  }
  if bytes[0] == bytes[1] && bytes[1] != bytes[2] {
    return true;
  }
  if bytes[1] == bytes[2] && bytes[0] != bytes[1] && bytes[2] != bytes[3] {
    return true;
  }
  if bytes[2] == bytes[3] && bytes[1] != bytes[2] && bytes[3] != bytes[4] {
    return true;
  }
  if bytes[3] == bytes[4] && bytes[2] != bytes[3] && bytes[4] != bytes[5] {
    return true;
  }
  if bytes[4] == bytes[5] && bytes[3] != bytes[4] {
    return true;
  }
  return false;
}