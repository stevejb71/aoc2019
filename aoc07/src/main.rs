use permutate::Permutator;
use std::fs;
use std::collections::HashSet;

fn main() {
  let data = read_file("./input.txt");
  part2(&data);
}

#[derive(Debug, Copy, Clone)]
struct Amplifier {
  pc: usize,
  running: bool,
  last_output: i32
}

impl Amplifier {
  pub fn new() -> Amplifier {
    Amplifier {pc: 0, running: true, last_output: 0}
  }
}

fn part2(data: &Vec<i32>) {
  let mut max = 0;
  let list: &[&u32] = &[&5, &6, &7, &8, &9];
  let lists = [list];
  let mut permutator = Permutator::new(&lists[..]);
  let mut amplifiers = vec![Amplifier::new(), Amplifier::new(), Amplifier::new(), Amplifier::new(), Amplifier::new()];
  while let Some(permutation) = permutator.next() {
    let phase_settings = permutation.iter().map(|&x| *x as i32).collect::<Vec<_>>();
    let phase_settings_set = phase_settings.iter().collect::<HashSet<_>>();
    if phase_settings_set.len() == list.len() {
      let output = run_amplifiers(&data, &phase_settings, &mut amplifiers);
      if max < output {
        max = output;
      }
    }
  }
  println!("Part 2: {}", max);
}

fn run_amplifiers(program: &Vec<i32>, phase_settings: &Vec<i32>, amplifiers: &mut Vec<Amplifier>) -> i32 {
  let mut input: i32 = 0;
  for (i, &phase_setting) in phase_settings.iter().enumerate() {
    let inputs = vec![input, phase_setting];
    run(&program, &inputs, &mut amplifiers[i]);
    input = &amplifiers[i].last_output;
  }
  while is_running(&amplifiers[4]) {
    for mut amplifier in amplifiers {
      run(&program, &vec![input], &mut amplifier);
      input = amplifier.last_output;
    }
  }
  amplifiers[4].last_output
}

fn is_running(a: &Amplifier) -> bool {
  return a.running;
}

fn read_file(filename: &str) -> Vec<i32> {
  fs::read_to_string(filename).unwrap()
      .split(",")
      .map(|x| x.parse().unwrap())
      .collect()
}

#[derive(PartialEq, Debug)]
enum Mode {
  Position,
  Immediate
}

fn run(program: &Vec<i32>, inputs: &Vec<i32>, amplifier: &mut Amplifier) {
  if !amplifier.running {
    return;
  }
  let mut copy = program.clone();
  let mut inputs_mut = inputs.clone();
  let mut has_output = false;
  while !has_output && copy[amplifier.pc] != 99 {
    amplifier.pc = process_op(&mut copy, &mut inputs_mut, amplifier.pc, |x| { amplifier.last_output = x; has_output = true; } );
  }
  if copy[amplifier.pc] == 99 {
    amplifier.running = false;
  }
}

fn process_op<F>(data: &mut Vec<i32>, inputs: &mut Vec<i32>, pc: usize, mut output: F) -> usize where F:FnMut(i32) -> () {
  let op = opcode(data[pc]);
  let modes = modes(data[pc] as usize);
  let read = {|index| read(data, &modes, pc, index)};
  match op {
    1 => {
      let op_code1 = read(1);
      let op_code2 = read(2);
      let out_index = data[pc+3] as usize;
      data[out_index] = op_code1 + op_code2;
      pc + 4
    },
    2 => {
      let op_code1 = read(1);
      let op_code2 = read(2);
      let out_index = data[pc+3] as usize;
      data[out_index] = op_code1 * op_code2;
      pc + 4
    }
    3 => {
      let out_index = data[pc+1] as usize;
      data[out_index] = inputs.pop().expect("no input");
      pc + 2
    },
    4 => {
      let op_code1 = read(1);
      output(op_code1);
      pc + 2
    },
    5 => {
      let op_code1 = read(1);
      if op_code1 != 0 {
        read(2) as usize
      } else {
        pc + 3
      }
    },
    6 => {
      let op_code1 = read(1);
      if op_code1 == 0 {
        read(2) as usize
      } else {
        pc + 3
      }
    },
    7 => {
      let op_code1 = read(1);
      let op_code2 = read(2);
      let out_index = data[pc+3] as usize;
      let value = if op_code1 < op_code2 {
        1
      } else {
        0
      };
      data[out_index] = value;
      pc + 4
    },
    8 => {
      let op_code1 = read(1);
      let op_code2 = read(2);
      let out_index = data[pc+3] as usize;
      let value = if op_code1 == op_code2 {
        1
      } else {
        0
      };
      data[out_index] = value;
      pc + 4
    },
    _ => panic!("Unhandled op: {} at pc {}", op, pc)
  }
}

fn read(data: &Vec<i32>, (a, b, c): &(Mode, Mode, Mode), pc: usize, index: usize) -> i32 {
  let mode = match index {
    1 => c,
    2 => b,
    3 => a,
    _ => panic!("Unhandled index for mode {}", index)
  };
  let param = data[pc+index];
  if *mode == Mode::Immediate {
    param
  } else {
    data[param as usize]
  }
}

fn opcode(instruction: i32) -> usize {
  (instruction % 10) as usize
}

fn modes(instruction: usize) -> (Mode, Mode, Mode) {
  let c = to_mode((instruction / 100) % 10);
  let b = to_mode((instruction / 1000) % 10);
  let a = to_mode((instruction / 10000) % 10);
  (a, b, c)
}

fn to_mode(x: usize) -> Mode {
  match x {
    0 => Mode::Position,
    1 => Mode::Immediate,
    _ => panic!("Unhandled mode {}", x)
  }
}