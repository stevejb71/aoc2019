use std::fs;

fn read(filename: &str) -> Vec<usize> {
  fs::read_to_string(filename).unwrap()
      .split(",")
      .map(|x| x.parse().unwrap())
      .collect()
}

fn main() {
  let data = read("./input.txt");
  part1(&data);
  part2(&data);
}

fn part1(program: &Vec<usize>) {
  println!("Part 1: {}", run(&program, 12, 2));
}

fn part2(program: &Vec<usize>) {
  for noun in 0..100 {
    for verb in 0..100 {
      if run(program, noun, verb) == 19690720 {
        println!("Part 2: {}", 100 * noun + verb);
        return;
      }
    }
  }
}

fn run(program: &Vec<usize>, noun: usize, verb: usize) -> usize {
  let mut copy = program.clone();
  copy[1] = noun;
  copy[2] = verb;
  let mut pc = 0;
  while copy[pc] != 99 {
    pc = process_op(&mut copy, pc);
  }
  copy[0]
}

fn process_op(data: &mut Vec<usize>, pc: usize) -> usize {
  let op = data[pc];
  let op_code1 = data[data[pc + 1]];
  let op_code2 = data[data[pc + 2]];
  let out_index = data[pc + 3];
  let result = match op {
    1 => op_code1 + op_code2,
    2 => op_code1 * op_code2,
    _ => panic!()
  };
  data[out_index] = result;
  pc + 4
}